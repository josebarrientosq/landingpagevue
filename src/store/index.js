import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import axios from "axios"

export default new Vuex.Store({
  state: {
    site: {
      site_id: '',
      site_titulo:'',
      user_id : '',
      password : '',
      email: ''
    },
    sites: [],
    user:{
      user_id : 'user'
    }
  },
  mutations: {
    getUser(state){
      return state.user
    },
    getSites(state,payload){
      state.sites= payload
    },
    getUrlAdmin(state){
      return "http://3.128.178.56"+state.site_id
    }
  },
  actions: {
    async crearSite({commit,state}){
      console.log("creeedaaando")
      try{
        console.log("creando site")
        let datos = await axios.post('https://gcvxevana6.execute-api.us-east-2.amazonaws.com/dev/crearSite',JSON.stringify(state.site))
        console.log (datos)
      }catch(error){
        console.log(error)

      }
    },
    async consultarSites({commit,state}){
      try{
        console.log("consultando")
        let user= state.user
        //let user = commit('getUser')
        console.log(user)
        let datos = await axios.post('https://gcvxevana6.execute-api.us-east-2.amazonaws.com/dev/listarSite',JSON.stringify(user))
        console.log (datos)
        commit('getSites', datos.data.Items)
      }catch(error){
        console.log(error)
      }
    },
    abrir_wp_admin({commit},siteId){
      console.log(siteId)
      var url = "http://3.128.178.56/"+siteId
      window.open(url, "_blank")
    },
    async borrar_site({commit},siteId,userId,index){
      console.log("Eliminando site")
      let payload={
        user_id : 'user',
        site_id : siteId
      }
      try{
        let datos = await axios.post('https://gcvxevana6.execute-api.us-east-2.amazonaws.com/dev/borrarSite',JSON.stringify(payload))
      }catch(error){
        console.log(error)
      }
      
    
    }
  },
  modules: {
  }
})
